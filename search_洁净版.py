# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util
import searchAgents


class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return [s, s, w, s, w, w, s, w]


def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.
    A sample depth first search implementation is provided for you to help you understand how to interact with the problem.
    """

    mystack = util.Stack()
    startState = (problem.getStartState(), '', 0, [])
    node, action, cost, path = startState
    mystack.push(startState)
    visited = set()
    while mystack:
        state = mystack.pop()
        node, action, cost, path = state
        if node not in visited:
            visited.add(node)
            if problem.isGoalState(node):
                path = path + [(node, action)]
                break;
            succStates = problem.getSuccessors(node)
            for succState in succStates:
                succNode, succAction, succCost = succState
                newstate = (succNode, succAction, cost + succCost, path + [(node, action)])
                mystack.push(newstate)
    actions = [action[1] for action in path]
    del actions[0]
    return actions


def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"
    queue = util.Queue()
    queue.push((problem.getStartState(), [], list()))
    while not queue.isEmpty():
        node, actions, visited = queue.pop()
        if node in visited: continue
        if problem.isGoalState(node): return actions
        visited.append(node)
        for coord, direction, _ in problem.getSuccessors(node):
            queue.push((coord, actions + [direction], visited))
    return []
    util.raiseNotDefined()


def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"
    priorityQueue = util.PriorityQueue()
    priorityQueue.push((problem.getStartState(), [], list()), 0)
    while not priorityQueue.isEmpty():
        node, actions, visited = priorityQueue.pop()
        if node in visited: continue
        if problem.isGoalState(node): return actions
        visited.append(node)
        for coord, direction, _ in problem.getSuccessors(node):
            n_actions = actions + [direction]
            print(n_actions)
            print(problem.getCostOfActions(n_actions))
            priorityQueue.push((coord, n_actions, visited), problem.getCostOfActions(n_actions))
    return []
    util.raiseNotDefined()


def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0


def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    heuristic = searchAgents.manhattanHeuristic
    priorityQueue = util.PriorityQueue()
    priorityQueue.push((problem.getStartState(), [], list()), 0)
    while not priorityQueue.isEmpty():
        node, actions, visited = priorityQueue.pop()
        if node in visited: continue
        if problem.isGoalState(node): return actions
        visited.append(node)
        for coord, direction, _ in problem.getSuccessors(node):
            n_actions = actions + [direction]
            cost = problem.getCostOfActions(n_actions) + heuristic(coord, problem)
            priorityQueue.push((coord, n_actions, visited), cost)
    return []
    util.raiseNotDefined()


def idaStarSearch(problem, heuristic=nullHeuristic, FOUND=None):
    """COMP90054 your solution to part 2 here """
    heuristic = searchAgents.manhattanHeuristic
    bound = heuristic(problem.getStartState(), problem)
    path = [(problem.getStartState(), '')]
    myStack = util.Stack()

    '''
    path是由tuple组成的list
    第一个元素 (5,5), ['']
    第二个元素 (4,5), ['', 'south']
    '''
    var = 1
    while var == 1:
        t = search(path, 0, bound, problem, myStack, heuristic=nullHeuristic, FOUND=None)
        if t == FOUND:
            node, action, cost, path = myStack.pop()
            actions = [action[1] for action in path]
            del actions[0]
            return actions
        bound = t
    util.raiseNotDefined()


def search(path, g, bound, problem, myStack, heuristic=nullHeuristic, FOUND=None):
    heuristic = searchAgents.manhattanHeuristic
    node = path[-1][0]
    count = 0
    # print('开始search，count = 0, bound是：' + str(bound))
    # print('node： ' + str(node))
    # print('g： ' + str(g))

    f = g + heuristic(node, problem)
    # print('f： ' + str(f))

    if f > bound:
        # print('f > bound, return f ')
        return f
    if problem.isGoalState(node):
        return FOUND
    min = float('inf')

    for succState in problem.getSuccessors(node):
        count += 1
        # print('count是： ' + str(count))
        # print('succ元素： ' + str(succState))
        succNode, succAction, succCost = succState
        accNode = [action[0] for action in path]
        if succNode not in accNode:
            # print('succ元素不在')
            path = path + [(succNode, succAction)]
            newState = (succNode, succAction, succCost, path)
            myStack.push(newState)
            t = search(path, g + succCost, bound, problem, myStack, heuristic=nullHeuristic, FOUND=None)
            # print('t: ' + str(t))
            if t == FOUND:
                # print('找到了')
                # print('list中的path是：' + str(path))
                # node, action, cost, path = myStack.pop()
                # print('stack中的path是：' + str(path))
                # myStack.push((node, action, cost, path))
                return FOUND
            # print('min: ' + str(min))

            if t < min:
                min = t
                # print('min被更小的t赋值：' + str(min))
            tryin = path.pop()
    #       print('pop掉的元素： ' + str(tryin))
    # print('结束for循环后的min: ' + str(min))
    return min

    util.raiseNotDefined()


def enforcedHillClimbing(problem, heuristic=nullHeuristic):
    startState = (problem.getStartState(), '', 0, [])
    node, action, cost, path = startState

    while not problem.isGoalState(node):
        startState = improve(startState, problem)
        node, action, cost, path = startState
    path = path + [(node, action)]
    actions = [action[1] for action in path]
    del actions[0]
    return actions
    util.raiseNotDefined()


def improve(startState, problem):
    myqueue = util.Queue()
    myqueue.push(startState)
    visited = set()
    node, action, cost, path = startState
    Dis = searchAgents.manhattanHeuristic(node, problem)
    while myqueue:
        popState = myqueue.pop()
        node, action, cost, path = popState
        cost = searchAgents.manhattanHeuristic(node, problem)
        if node not in visited:
            visited.add(node)
            if cost < Dis:
                Dis = cost
                return popState
            succStates = problem.getSuccessors(node)
            for succState in succStates:
                succNode, succAction, succCost = succState
                succCost = searchAgents.manhattanHeuristic(succNode, problem)
                newState = (succNode, succAction, succCost, path + [(node, action)])
                myqueue.push(newState)

    util.raiseNotDefined()


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
ehc = enforcedHillClimbing
ida = idaStarSearch
